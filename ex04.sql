drop table if exists produce;
drop table if exists act;
drop table if exists producer;
drop table if exists film;
drop table if exists actor;

--actor (name)
create table actor (
	name varchar(30),
	primary key (name)
);

-- film (title, genre)
create table film (
	title varchar(50),
	genre varchar(20),
	primary key (title)
);

--producer (name)
create table producer (
	name varchar(30),
	primary key (name)
);

--act (name, title, role)
create table act (
	name varchar(30),
	title varchar(50),
	role varchar(20),
	primary key (name, title, role),
	foreign key (name) references actor (name),
	foreign key (title) references film (title)
);

--produce (name, title)
create table produce (
	name varchar(30),
	title varchar(50),
	primary key (name, title),
	foreign key (name) references producer (name),
	foreign key (title) references film (title)
);

insert into actor values
	('Eddie Murphy'),
	('Adam Sandler');

insert into film values
	('Coming to America', 'Comedy'),
	('Happy Gilmore', 'Comedy'),
	('50 First Dates', 'Dramedy');

insert into producer values
	('Big Shot Producer'),
	('Mr. Producer man');
	
insert into act values
	('Eddie Murphy', 'Coming to America', 'The Prince'),
	('Eddie Murphy', 'Coming to America', 'Barber'),
	('Adam Sandler', 'Happy Gilmore', 'Happy Gilmore'),
	('Adam Sandler', '50 First Dates', 'Hopeless Romantic');

insert into produce values
	('Big Shot Producer', 'Happy Gilmore'),
	('Big Shot Producer', '50 First Dates'),
	('Mr. Producer man', 'Coming to America');
