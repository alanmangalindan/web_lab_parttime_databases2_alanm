--1. What are the names of the students who attend COMP219?
select s.fname, s.lname from unidb_students as s, unidb_attend as a
where a.id = s.id
and dept = 'comp'
and num = '219';

--2. What are the names of the student reps who are not from NZ?
select s.fname, s.lname from unidb_students as s, unidb_courses as c
where s.id = c.rep_id
and country != 'NZ';

--3. What are the offices for the lecturers of COMP219?
select office from unidb_lecturers as l, unidb_teach as t
where l.staff_no = t.staff_no
and dept = 'comp'
and num = '219';

--4. What are the names of the students taught by Te Taka?
select distinct s.fname, s.lname from unidb_lecturers as l, unidb_teach as t, unidb_attend as a, unidb_students as s
where l.staff_no = t.staff_no
and a.dept = t.dept
and a.num = t.num
and a.id = s.id
and l.fname = 'Te Taka';

--5. List each student (first & last name), along with their mentor (first & last name). The four columns in the result table should be “First name”, “Surname”, “Mentor first name”, and “Mentor surname”. The table should be ordered by the mentor surname, in ascending order.
select s1.fname'First name', s1.lname 'surname', s2.fname 'Mentor first name', s2.lname 'Mentor surname' from unidb_students s1, unidb_students s2
where s1.mentor = s2.id
order by s2.lname asc;

--6. Name all lecturers whose office is in G block, AND all students who are not from NZ (in the same result table).
select l.fname, l.lname from unidb_lecturers l where office like 'G%'
union
select s.fname, s.lname from unidb_students s where country != 'NZ';

--7. Name the course co-ordinator and student rep for COMP219.
select l.fname 'Co-ordinator first name', l.lname 'Co-ordinator surname', s.fname 'Student Rep First name', s.lname 'Student Rep surname' from unidb_courses c, unidb_lecturers l, unidb_students s
where c.coord_no = l.staff_no
and c.rep_id = s.id
and c.dept = 'comp'
and c.num = '219';

-- Give me the name of each student, along with the number of courses they take. Order by last name of students.
select s.fname, s.lname, count(*) 'Num courses taken' from unidb_students as s, unidb_attend as a
where s.id = a.id
group by s.fname, s.lname
order by s.lname;