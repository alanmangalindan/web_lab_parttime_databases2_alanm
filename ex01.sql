--1. Display the departments offering courses.
select distinct dept from unidb_courses;

--2. Display the semesters being attended.
select distinct semester from unidb_attend;

--3. Display the courses that are attended.
select dept, num, semester, count(id) from unidb_attend
group by dept, num, semester;

--4. List the students’ first names, last names, and country, ordered by first name.
select fname, lname, country from unidb_students
order by fname;

--5. List all lecturers, ordered by their office.
select * from unidb_lecturers order by office;

--6. List all staff whose staff number is greater than 500.
select * from unidb_lecturers where staff_no >500;

--7. List all students whose id is greater than 1668 and less than 1824.
select * from unidb_students where id >1668 and id < 1824;

--8. List the students from NZ, Australia and USA.
select * from unidb_students where country in ('NZ', 'AU', 'US');

--9. List all the lecturers in G block.
select * from unidb_lecturers where office like 'G%';

--10. List the courses NOT from the computer science department.
select * from unidb_courses where dept != 'comp';