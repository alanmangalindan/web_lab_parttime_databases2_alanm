drop table if exists location;
drop table if exists activity_taken;
drop table if exists activity;
drop table if exists student;

--student (student_id, first name, last name)
create table student (
	student_id int,
	fname varchar(30),
	lname varchar(30),
	primary key (student_id)
);

--activity (sports_id, name)
create table activity (
	sports_id int,
	name varchar(30),
	primary key (sports_id)
);

--location (loc_name, week, sports_id)
create table location (
	loc_name varchar(30),
	week int,
	sports_id int,
	check (week >= 1 and week <= 52),
	primary key (loc_name, week),
	foreign key (sports_id) references activity (sports_id)
);

--activity_taken (student_id, sports_id, semester, cost)
create table activity_taken (
	student_id int,
	sports_id int,
	semester varchar(10),
	cost int,
	primary key (student_id, sports_id, semester),
	foreign key (student_id) references student (student_id),
	foreign key (sports_id) references activity (sports_id)
);

insert into student values
	(0, 'Fred', 'Flintstone'),
	(1, 'Wilma', 'Flintstone'),
	(2, 'Barney', 'Rubble'),
	(3, 'Betty', 'Rubble');

insert into activity values
	(0, 'Rugby'),
	(1, 'Basketball'),
	(2, 'Tennis'),
	(3, 'Football'),
	(4, 'Volleyball'),
	(5, 'Netball');

insert into activity_taken values
	(0, 0, '2018B', 15),
	(0, 1, '2018B', 20),
	(1, 4, '2018B', 25),
	(1, 5, '2019A', 25),
	(2, 2, '2018B', 40),
	(2, 3, '2019A', 35),
	(3, 3, '2018B', 30),
	(3, 3, '2019A', 35);

insert into location values
	('Rugby Field', 45, 0),
	('Basketball Court', 45, 1),
	('Tennis Court', 45, 2),
	('Football Field', 45, 3),
	('Volleyball Court', 45, 4),
	('Netball Court', 45, 5);

insert into location values ('Volleyball Court', 45, 0);
	
	

