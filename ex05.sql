drop table if exists members;
drop table if exists teams;
drop table if exists players;

--players (name, player_num, age, nationality)
create table players (
	name varchar(30),
	player_num int,
	age int,
	nationality varchar(15),
	primary key (player_num)
);

--teams (name, team_num, home_city, captain, points)
create table teams (
	name varchar(30),
	team_num int,
	home_city varchar(20),
	captain int,
	points int,
	primary key (name, team_num),
	foreign key (captain) references players (player_num)
);

--members (name, team_num, player_num)
create table members (
	name varchar(30),
	team_num int,
	player_num int,
	primary key (name, team_num, player_num),
	foreign key (name, team_num) references teams (name, team_num),
	foreign key (player_num) references players (player_num)
);

insert into players values
	('Cristiano Ronaldo', 0, 30, 'Portuguese'),
	('Lionel Messi', 1, 30, 'Argentinian'),
	('Neymar Jr', 2, 30, 'Brazilian'),
	('Wayne Rooney', 3, 30, 'English');

insert into teams values
	('Juventus', 0, 'Rome', 0, 1000),
	('Manchester United', 1, 'Manchester', 3, 750),
	('Paris St Germain', 2, 'Paris', 2, 900);

insert into members values
	('Juventus', 0, 0),
	('Juventus', 0, 1),
	('Paris St Germain', 2, 2),
	('Manchester United', 1, 3);
